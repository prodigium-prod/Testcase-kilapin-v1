const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const categorySchema = new mongoose.Schema({
  name: { type: String, required: true },
  service_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Service', required: true },
});

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;
