const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const priceSchema = new mongoose.Schema({
  category_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Category', required: true },
  price: { type: Number, required: true },
  status: { type: String, required: true },
});

const Price = mongoose.model('Price', priceSchema);

module.exports = Price;
