const mongoose = require('mongoose');

const hubSchema = new mongoose.Schema({
  order_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Order', required: true },
  cleaner_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  assigner: { type: String, required: true },
  decision_date: { type: Date, default: Date.now },
  postal_code: { type: String, required: true },
});

const Hub = mongoose.model('Hub', hubSchema);

module.exports = Hub;
