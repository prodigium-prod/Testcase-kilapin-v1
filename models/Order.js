const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const orderSchema = new mongoose.Schema({
  user_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  service_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Service', required: true },
  category_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Category', required: true },
  booking_type: { type: String, required: true },
  cleaner_id: { type: String},
  status: { type: String, required: true },
  price: { type: Number },
  description: { type: String },
  rating: { type: Number },
  review: { type: String },
});

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
