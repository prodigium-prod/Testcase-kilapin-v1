const mongoose = require('mongoose');
const Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

const serviceSchema = new mongoose.Schema({
  name: { type: String, required: true },
  description: { type: String },
  status: { type: String, required: true },
});

const Service = mongoose.model('Service', serviceSchema);

module.exports = Service;
