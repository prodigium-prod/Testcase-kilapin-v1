const mongoose = require('mongoose');
const User = require('../models/User'); // Import your User model

// Connect to MongoDB
mongoose.connect('mongodb+srv://kilapin:Ifvu1ovfqbhXFTRE@cluster0.oucnaua.mongodb.net/kilapin', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Create a data seed for a user with specific fields
async function createDataSeed() {
  try {
    // Create a new user document
    const newUser = new User({
      name: 'Admin',
      email: 'admin@example.com',
      password: '12345',
      phone: '1234567890',
      role: 'Admin',
    });

    // Save the user to the database
    await newUser.save();
    console.log('Data seed created successfully.');

    // Disconnect from MongoDB
    mongoose.disconnect();
  } catch (error) {
    console.error('Error creating data seed:', error);
  }
}

// Call the function to create the data seed
createDataSeed();
