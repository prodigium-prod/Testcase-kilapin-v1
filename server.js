const express = require('express');
const mongoose = require('mongoose');


// Import your models here
const User = require('./models/User');
// const Service = require('./models/service');
// const Category = require('./models/category');
// const Price = require('./models/price');
// const Order = require('./models/order');
// const Payment = require('./models/payment');
// const Notification = require('./models/notification');
// const News = require('./models/news');

// Create an instance of Express
const app = express();

// Configure the port
const port = process.env.PORT || 3000;

// Configure the MongoDB Atlas connection URI
const dbURI = 'mongodb+srv://kilapin:Ifvu1ovfqbhXFTRE@cluster0.oucnaua.mongodb.net/kilapin';

// Connect to MongoDB Atlas
mongoose.connect(dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
  .then(() => {
    console.log('Connected to MongoDB Atlas');
    // Start the server after successful connection
    app.listen(port, () => {
      console.log(`Server is running on port ${port}`);
    });
  })
  .catch((err) => {
    console.error('Error connecting to MongoDB Atlas:', err);
  });

// Middleware to parse JSON bodies
app.use(express.json());

// Define your routes here
// app.use('/users', require('./routes/userRoutes'));
// app.use('/services', require('./routes/serviceRoutes'));
// app.use('/categories', require('./routes/categoryRoutes'));
// app.use('/prices', require('./routes/priceRoutes'));
// app.use('/orders', require('./routes/orderRoutes'));
// app.use('/payments', require('./routes/paymentRoutes'));
// app.use('/notifications', require('./routes/notificationRoutes'));
// app.use('/news', require('./routes/newsRoutes'));

// Handle undefined routes
app.use((req, res) => {
  res.status(404).json({ message: 'Not Found' });
});

// Handle server errors
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).json({ message: 'Internal Server Error' });
});
