const mongoose = require('mongoose');
const User = require('../models/User');
const Service = require('../models/Service');
const Category = require('../models/Category');
const Order = require('../models/Order');

describe('User Signup and Order Creation', () => {
  beforeAll(async () => {
    // Connect to the MongoDB test database
    await mongoose.connect('mongodb+srv://kilapin:Ifvu1ovfqbhXFTRE@cluster0.oucnaua.mongodb.net/test', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    // Clear the collections before running the tests
    await User.deleteMany({});
    await Service.deleteMany({});
    await Category.deleteMany({});
    await Order.deleteMany({});
  });

  afterAll(async () => {
    // Disconnect from the MongoDB test database
    await mongoose.disconnect();
  });

  it('Bagas acts as hub Admin and registers', async () => {
    // Create a new user
    const userBagas = new User({
      name: 'Bagas',
      email: 'bagas@example.com',
      password: 'password',
      phone: '123456789',
      role: 'Hub',
      postalcode: '50272',
    });

    // Save the user to the database
    await userBagas.save();
    console.log('Bagas as Hub Admin created!');
  });

  it('Kevin acts as Cleaner with postal code 50272 and registers', async () => {
    // Create a new user
    const userKevin = new User({
      name: 'Kevin',
      email: 'kevin@example.com',
      password: 'password',
      phone: '123456789',
      role: 'Hub',
      postalcode: '50272',
    });

    // Save the user to the database
    await userKevin.save();
    console.log('Kevin as Cleaner 50272 created!');
    console.log(userKevin);
  });

  it('Revan acts as Cleaner with postal code 50273 and registers', async () => {
    // Create a new user
    const userRevan = new User({
      name: 'Revan',
      email: 'revan@example.com',
      password: 'password',
      phone: '123456789',
      role: 'Hub',
      postalcode: '50273',
    });

    // Save the user to the database
    await userRevan.save();
    console.log('Revan as Cleaner 50273 created!');
  });

  it('Berry acts as Cleaner with postal code 50274 and registers', async () => {
    // Create a new user
    const userBerry = new User({
      name: 'Berry',
      email: 'berry@example.com',
      password: 'password',
      phone: '123456789',
      role: 'Hub',
      postalcode: '50274',
    });

    // Save the user to the database
    await userBerry.save();
    console.log('Berry as Cleaner 50274 created!');
  });

  it('Budi acts as Customer and registers successfully', async () => {
    // Create a new user
    const userBudi = new User({
      name: 'Budi',
      email: 'budi@example.com',
      password: 'password',
      phone: '123456789',
      role: 'Customer',
      postalcode: '50272',
    });
    // Save the user to the database
    await userBudi.save();
    console.log('Budi as Customer 50272 created!');
  });

  it('should create Services and Categories', async () => {
    // Create new Services
    const service1 = new Service({
      name: 'kilapin cleaner',
      description: 'Cleaning service',
      status: 'Active',
    });

    const service2 = new Service({
      name: 'kilapin repair',
      description: 'Repair service',
      status: 'Active',
    });

    const service3 = new Service({
      name: 'kilapin fintech',
      description: 'Fintech service',
      status: 'Active',
    });

    // Save the services to the database
    await Promise.all([service1.save(), service2.save(), service3.save()]);

    // Create the categories
    const category1 = new Category({
      name: 'booking',
      service_id: service1._id,
    });

    const category2 = new Category({
      name: 'urgent',
      service_id: service2._id,
    });

    // Save the categories to the database
    await Promise.all([category1.save(), category2.save()]);
  });

  it('should create an Order and assign a Cleaner', async () => {
    // Create an order
    const userBudi = await User.findOne({ name: 'Budi' });
    const service1 = await Service.findOne({ name: 'kilapin cleaner' });
    const category1 = await Category.findOne({ name: 'booking' });

    const order = new Order({
      user_id: userBudi._id,
      service_id: service1._id,
      category_id: category1._id,
      booking_type: 'booking',
      cleaner_id: '', // Assuming the user is also the cleaner for this order
      status: 'Pending',
      price: 100.0,
      description: 'Cleaning service for my home',
      rating: 0,
      review: '',
    });

    await order.save();

    // Find a cleaner with the matching postal code
    const cleaner = await User.findOne({ postalcode: userBudi.postalcode });

    // Update the order with the assigned cleaner_id
    order.cleaner_id = cleaner._id;
    await order.save();

    console.log('Order created and cleaner assigned!');
    console.log('Order:', order);
  });
});
